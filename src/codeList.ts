import { C } from "../classNames.ts";
import { DLsiteWorkCode } from "./DLsite.ts";
import { html } from "./utils/html.ts";
import { nodePositionComparator } from "./utils/various.ts";

let root: HTMLElement;
let enabled = false;
let currentCode: DLsiteWorkCode;
let mentionIndex = 0;
let scheduledAnimationFrame = false;
let list: DLsiteWorkCode[] = [];
let lastSize = 0;

function onMouseUp(event: PointerEvent, code: DLsiteWorkCode) {
  if (event.button !== 0) return;
  if (currentCode !== code) {
    currentCode = code;
    mentionIndex = 0;
  } else {
    mentionIndex = mentionIndex < code.mentions.length - 1
      ? mentionIndex + 1
      : 0;
  }
  const mention = code.mentions[mentionIndex]!;
  console.debug("Focusing mention: %o", mention);
  mention.focus();
  mention.classList.toggle(C.FOCUSED, true);
  mention.addEventListener(
    "focusout",
    () => mention.classList.toggle(C.FOCUSED, false),
    { once: true },
  );
}

function getClasses({ invalid }: DLsiteWorkCode) {
  return `${C.LINK} ${
    !invalid ? C.PREVIEWABLE : C.INVALID
  } ${C.DLSITE_WORK} ${C.LIST_ITEM}`;
}

function getBody({ code, mentions }: DLsiteWorkCode) {
  return `${code}${mentions.length > 1 ? ` (${mentions.length})` : ""}`;
}

function getOrderedList(): DLsiteWorkCode[] {
  return Array.from(DLsiteWorkCode.list.values())
    .filter((a) => a.mentions.length > 0)
    .sort((a, b) => nodePositionComparator(a.mentions[0]!, b.mentions[0]!));
}

function toggle(): void {
  if (!enabled) {
    root = html`<div class=${C.LIST}></div>` as HTMLElement;
    document.body.append(root);
    // eslint-disable-next-line no-use-before-define
    renderList();
  } else {
    root.remove();
  }
  enabled = !enabled;
}

function renderList() {
  performance.mark("renderCodeListStart");
  const unique = DLsiteWorkCode.uniqueMentionCount;
  const total = DLsiteWorkCode.totalMentionCount;
  const title = `${unique} unique codes (${total} in total)`;
  if (unique > lastSize) {
    list = getOrderedList();
    lastSize = unique;
  }
  root.replaceChildren(
    html`
      <a class="${C.LIST_ITEM} ${C.LIST_HIDER}" on:click=${toggle}>[hide]</a>
      ${
      list.map((code) => {
        return html`<a
          class="${getClasses(code)}"
          href="${code.getWorkUrl()}"
          rel="noreferrer"
          target="_blank"
          data-code="${code}"
          on:mousedown=${(event: PointerEvent) =>
          event.preventDefault() /* Middle mouse button scroll. */}
          on:click=${(event: PointerEvent) =>
          event.preventDefault() /* Primary navigation. */}
          on:mouseup=${(event: PointerEvent) => onMouseUp(event, code)}
          >${getBody(code)}</a
        >`;
      })
    }
      <span class="${C.LIST_TOTAL}" title="${title}">(${unique}/${total})</span>
    `,
  );

  const m = performance.measure("renderCodeList", "renderCodeListStart");
  console.debug(`Rendering code list took ${m.duration}ms`);
}

export const CodeList = {
  toggle,
  invalidate(): void {
    if (scheduledAnimationFrame || !enabled) {
      return;
    }
    scheduledAnimationFrame = true;
    requestAnimationFrame(() => {
      renderList();
      scheduledAnimationFrame = false;
    });
  },
  registerMenu(): void {
    GM.registerMenuCommand("Toggle code list", CodeList.toggle);
  },
};
