import { C } from "../classNames.ts";
import {
  getCreatorUrl,
  getImageUrl,
  getWorkUrl,
  guessSite,
  normalizeMakerCode,
  normalizeWorkCode,
} from "./lib/DLsite/mod.ts";
import {
  Ana,
  Fumilink,
  MakerCode,
  MakerCodeGroups,
  MaybePromise,
  Site,
  WorkCode,
  WorkCodeLinkGroups,
} from "./types/mod.ts";
import { defer } from "./utils/defer.ts";
import { monkeyfetch } from "./utils/monkeyfetch.ts";
import { sortedNodeInsertion } from "./utils/various.ts";

async function imageFetch(url: string) {
  const headers = { "X-Requested-With": "XMLHttpRequest" };
  return await monkeyfetch<Blob>({
    headers,
    method: "GET",
    responseType: "blob",
    url,
    anonymous: true,
  });
}

export abstract class DLsiteCode {
  abstract code: WorkCode | MakerCode;
  abstract site: Site;
  invalid = false;

  toString(): string {
    return this.code;
  }

  [Symbol.toPrimitive](hint: string): string | number {
    return hint === "number" ? +this.code.slice(2) : this.code;
  }

  getFumilinks(): NodeListOf<Fumilink> {
    return document
      .querySelectorAll<Fumilink>(`a.${C.LINK}[data-code=${this}]`);
  }
}

export class DLsiteWorkCode extends DLsiteCode {
  static list = new Map<string, DLsiteWorkCode>();
  static uniqueMentionCount = 0;
  static totalMentionCount = 0;
  code: WorkCode;
  site: Site;
  announce: boolean | undefined;
  #image_main: MaybePromise<string | null> = null;
  readonly mentions: HTMLAnchorElement[] = [];

  getMainImageUrl(): string {
    const { code, announce } = this;
    return getImageUrl(code, "main", announce);
  }

  getWorkUrl(): string {
    const { code, site, announce } = this;
    return getWorkUrl(code, site, announce);
  }

  #updateLinks() {
    console.debug("Updating links");
    const links = this.getFumilinks();
    const workUrl = this.getWorkUrl();
    links.forEach((elem) => (elem.href = workUrl));
  }

  #markInvalid() {
    console.debug("Marking the code as invalid");
    const links = this.getFumilinks();
    this.invalid = true;
    links
      .forEach((elem) => elem.classList.replace(C.PREVIEWABLE, C.INVALID));
  }

  /** Tries to fetch main image at both work and announce endpoints and marks
   * the code as invalid if neither works. */
  async getMainImage(): Promise<string | null> {
    if (this.invalid) {
      console.error(this);
      throw new Error("Attempted to fetch main image of an invalid code");
    }

    if (this.#image_main) {
      if (this.#image_main instanceof Promise) {
        console.debug(`${this} has a pending main image fetch: %o`, this);
      } else {
        console.debug(`Main image of ${this} was already fetched: %o`, this);
      }
      return this.#image_main;
    }

    console.debug(`Fetching main image of ${this}: %o`, this);
    const { promise, resolve } = defer<string | null>();
    this.#image_main = promise;
    try {
      for (let i = 1; i <= 2; i++) {
        const { response, status, ok } = await imageFetch(
          this.getMainImageUrl(),
        );
        if (!ok || !response) {
          console.debug(
            `Main image fetch failure status: ${status}\n${this.getMainImageUrl()} couldn't be fetched`,
          );
          this.announce = !this.announce;
          if (i >= 2) this.#markInvalid();
        } else {
          console.debug(`Succeeded after ${i === 1 ? "1 try" : `${i} tries`}`);
          if (i % 2 === 0) this.#updateLinks();
          this.#image_main = URL.createObjectURL(response);
          break;
        }
      }
    } catch (err) {
      console.error("Main image fetch error: %o", err);
    }
    if (this.#image_main instanceof Promise) {
      this.#image_main = null;
    }
    resolve(this.#image_main);
    return this.#image_main;
  }

  addMention(mention: HTMLAnchorElement): void {
    if (!this.mentions.length) {
      DLsiteWorkCode.uniqueMentionCount++;
    }
    sortedNodeInsertion(this.mentions, mention);
    DLsiteWorkCode.totalMentionCount++;
  }

  private constructor(
    code: WorkCode,
    site: Site | undefined,
    ana: Ana | undefined,
  ) {
    super();
    if (ana) {
      this.announce = ana === "announce";
    }
    this.code = code;
    this.site = site ?? guessSite(code);
    DLsiteWorkCode.list.set(code, this);
  }

  static get(data: WorkCodeLinkGroups): DLsiteWorkCode {
    const { site, ana, work_code } = data;
    if (!work_code) {
      throw new Error("No code");
    }
    const code = normalizeWorkCode(work_code);

    const existing = DLsiteWorkCode.list.get(code);
    if (existing) {
      /** Upgrade the code with accurate info from a link. */
      if (ana && existing.announce === undefined) {
        existing.site = site as Site;
        existing.announce = ana === "announce";
        const workUrl = existing.getWorkUrl();
        const links = existing.getFumilinks();
        links.forEach((elem) => (elem.href = workUrl));
      }
      return existing;
    } else return new DLsiteWorkCode(code, site, ana);
  }
}

export class DLsiteMakerCode extends DLsiteCode {
  static list = new Map<string, DLsiteMakerCode>();
  code: MakerCode;
  site: Site;
  name: string | undefined;
  icon: string | undefined;

  getCreatorUrl(): string {
    const { code, site } = this;
    return getCreatorUrl(code, site);
  }

  private constructor(code: MakerCode) {
    super();
    this.code = code;
    this.site = guessSite(this.code);
    DLsiteMakerCode.list.set(code, this);
  }

  static get(data: MakerCodeGroups): DLsiteMakerCode {
    const { maker_code } = data;
    if (!maker_code) {
      throw new Error("No code");
    }
    const code = normalizeMakerCode(maker_code);
    const existing = DLsiteMakerCode.list.get(code);
    if (existing) return existing;
    else return new DLsiteMakerCode(code);
  }
}
