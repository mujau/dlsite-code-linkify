import type { Tagged } from "./deps.ts";

export type WorkPrefix = "RJ" | "RE" | "VJ" | "BJ";
export type MakerPrefix = "RG" | "VG" | "BG";
export type Prefix = WorkPrefix | MakerPrefix;
export type RawWorkCode = Tagged<string, "RawWorkCode">;
export type WorkCode = Tagged<RawWorkCode, "WorkCode">;
export type RawMakerCode = Tagged<string, "RawMakerCode">;
export type MakerCode = Tagged<RawMakerCode, "MakerCode">;
export type Site =
  | "home"
  | "maniax"
  | "ecchi-eng"
  | "pro"
  | "books"
  | "ai"
  | "aix";
export type Ana = "announce" | "work";
export type SiteId = "doujin" | "professional" | "books";
/** General Audience | R15+ | R18+ */
export type AgeCategory = 1 | 2 | 3;

export type WorkCodeGroups = {
  work_code: RawWorkCode | undefined;
};

export type WorkCodeLinkGroups = {
  site?: Site | undefined;
  ana?: Ana | undefined;
} & WorkCodeGroups;

export type MakerCodeGroups = {
  maker_code: RawMakerCode | undefined;
};

export type MakerCodeLinkGroups = {
  site?: Site | undefined;
} & MakerCodeGroups;

export interface SuggestResponseItem {
  work: {
    work_name: string;
    workno: WorkCode;
    maker_name: string;
    maker_id: MakerCode;
    work_type: string; // TODO: maybe define it as a type
    intro_s: string;
    age_category: AgeCategory;
    is_ana: boolean;
  };
  maker: {
    workno: WorkCode;
    maker_name: string;
    maker_name_kana: string;
    maker_id: MakerCode;
    age_category: AgeCategory;
    is_ana: boolean;
  };
}

export interface SuggestResponse {
  work: SuggestResponseItem["work"][];
  maker: SuggestResponseItem["maker"][];
  reqtime: number;
}
