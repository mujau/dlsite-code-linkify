/* v1.0.0.0 2024-09-18 */
/** Merges an array of strings and `RegExp` into a single `RegExp`. */
export function mergeRegex(parts: (string | RegExp)[], flags = ""): RegExp {
  const patterns = [], flagSet = new Set(flags.split(""));
  for (const part of parts) {
    if (part instanceof RegExp) {
      const { source, flags } = part;
      patterns.push(source);
      flags.split("").forEach(
        (flag) => flagSet.add(flag),
      );
    } else patterns.push(part);
  }
  const finalFlags = Array.from(flagSet).join("");
  return new RegExp(patterns.join(""), finalFlags);
}

/** Splits a string at the given index and returns both parts. The parts are
 * ordered from the left even if index is negative. */
export function splitAt<A = string, B = string>(
  str: string,
  index: number,
): [A, B] {
  return [str.slice(0, index), str.slice(index)] as [A, B];
}
