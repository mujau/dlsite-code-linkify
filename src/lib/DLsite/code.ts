import {
  MakerCode,
  MakerPrefix,
  Prefix,
  RawMakerCode,
  RawWorkCode,
  Site,
  SiteId,
  WorkCode,
  WorkPrefix,
} from "./types.ts";
import { mergeRegex, splitAt } from "./utils.ts";

export const quickCodeRe = /[RBV][JGE]\d{4,8}/i;
const linkPrefixRe = /(?:https?:\/\/)?(?:www\.)?dlsite.com\/(?<site>[^\/]+)\//i;
const linkPrefixWorkPartRe = /(?<ana>work|announce)\/=\/product_id\//i;
const linkPrefixMakerPartRe = /circle\/profile\/=\/maker_id\//;
const linkPostfixRe = /(?:\.html)?/i;
export const workCodeRe =
  /(?<work_code>(?:[RVB]J|RE)(?:\d{8}|\d{6}|\d{4}))(?!\d)/i;
export const makerCodeRe = /(?<maker_code>(?:[RVB]G)(?:\d{8}|\d{4,5}))(?!\d)/i;
const workLinkPrefixRe = mergeRegex([
  linkPrefixRe,
  linkPrefixWorkPartRe,
]);
const makerLinkPrefixRe = mergeRegex([
  linkPrefixRe,
  linkPrefixMakerPartRe,
]);
const genericLinkPrefixRe = mergeRegex([
  linkPrefixRe,
  "(?:",
  linkPrefixWorkPartRe,
  "|",
  linkPrefixMakerPartRe,
  ")",
]);
export const workLinkRe = mergeRegex([
  workLinkPrefixRe,
  workCodeRe,
  linkPostfixRe,
]);
export const makerLinkRe = mergeRegex([
  makerLinkPrefixRe,
  makerCodeRe,
  linkPostfixRe,
]);
export const genericCodeRe = mergeRegex([
  "(?:",
  genericLinkPrefixRe,
  ")?(?:",
  workCodeRe,
  "|",
  makerCodeRe,
  ")",
]);

/** Best guess. Doesn't really matter anyway. */
export function guessSite(code: string): Site {
  const [prefix] = splitAt<Prefix>(code, 2);
  switch (prefix) {
    case "RJ":
    case "RG":
      return "maniax";
    case "RE":
      return "ecchi-eng";
    case "VJ":
    case "VG":
      return "pro";
    case "BJ":
    case "BG":
      return "books";
    default: {
      console.warn(`Unknown code prefix: \`${prefix}\``);
      return "home";
    }
  }
}

function getSiteId(prefix: WorkPrefix): SiteId {
  if (prefix[0] === "V") return "professional";
  if (prefix[0] === "B") return "books";
  return "doujin";
}

function getBucket(prefix: WorkPrefix, no: string): string {
  const [a, b] = splitAt(no, -3);
  const bucket = `${+a + (b === "000" ? 0 : 1)}`.padStart(a.length, "0");
  return `${prefix}${bucket}000`;
}

type ImageType = "main" | "mini";

export function getImageUrl(
  code: WorkCode,
  type: ImageType,
  announce = false,
): string {
  const [prefix, no] = splitAt<WorkPrefix>(code, 2);
  const siteId = getSiteId(prefix);
  const bucket = getBucket(prefix, no);
  const anaWork = announce ? "ana" : "work";
  const anaSuffix = announce ? "_ana" : "";
  const imgName = type === "main" ? "img_main" : "img_sam_mini";
  const imgFilename = `${code}${anaSuffix}_${imgName}.jpg`;
  return `https://img.dlsite.jp/modpub/images2/${anaWork}/${siteId}/${bucket}/${imgFilename}`;
}

export function getWorkUrl(
  code: WorkCode,
  site = guessSite(code),
  announce = false,
): string {
  const anaWork = announce ? "announce" : "work";
  return `https://www.dlsite.com/${site}/${anaWork}/=/product_id/${code}.html`;
}

export function getCreatorUrl(code: MakerCode, site = guessSite(code)): string {
  return `https://www.dlsite.com/${site}/circle/profile/=/maker_id/${code}.html`;
}

export function getCienLookupUrl(code: MakerCode): string {
  return `https://media.ci-en.jp/dlsite/lookup/${code}.json`;
}

export function getSuggestUrl(term: string): string {
  const ts = Number(new Date());
  return `https://www.dlsite.com/suggest/?term=${term}&site=adult-jp&time=${ts}`;
}

export function normalizeWorkCode(code: RawWorkCode): WorkCode {
  const [prefix, no] = splitAt<WorkPrefix>(code, 2);
  return prefix.toUpperCase() + no.padStart(6, "0") as WorkCode;
}

export function normalizeMakerCode(code: RawMakerCode): MakerCode {
  const [prefix, no] = splitAt<MakerPrefix>(code, 2);
  return prefix.toUpperCase() + no.padStart(5, "0") as MakerCode;
}
