import {} from "typed-query-selector";
import { CodeList } from "./codeList.ts";
import { addDebugStuff } from "./debug.ts";
import { initImagePreview } from "./fumiPreview.ts";
import { quickCodeRe } from "./lib/DLsite/code.ts";
import { augumentDLsiteLinks, linkify } from "./linkify.ts";
import { yonchan, yonchanDomains } from "./yonchan.ts";

function generic(): void {
  augumentDLsiteLinks(document.body);
  linkify(document.body, { branchPruning: true });
}

function router(): boolean {
  const hasCodes = quickCodeRe.test(document.body.textContent!);
  console.debug(`The page has codes: ${hasCodes}`);
  if (yonchanDomains.includes(location.hostname)) {
    // NOTE: Idle callback trick doesn't work as 4chan-xt is loaded as an async script.
    // I have no clue how to make use of this event on 4chan without making 4chan-x(t) mandatory.
    if (location.hostname === "4chan.gay") {
      addEventListener("4chanXInitFinished", () => yonchan(hasCodes), {
        once: true,
      });
    } else {
      yonchan(hasCodes);
    }
  } else if (hasCodes) {
    generic();
  } else {
    console.debug("Nothing to linkify");
    return false;
  }
  return true;
}

function init() {
  console.debug("dlsite-code-linkify init has started");
  const t1 = performance.now();
  const matched = router();
  const t2 = performance.now();
  console.debug(`Linkification took ${t2 - t1}ms`);
  if (!matched) return;

  GM.addStyle(__CSS_SLOT);
  initImagePreview();
  CodeList.registerMenu();
  __DEV && addDebugStuff();
}

/** Gives 4chanX enough time to finish whatever it does. Hopefully. */
requestIdleCallback(init);
