import { C } from "../classNames.ts";
import { DLsiteMakerCode, DLsiteWorkCode } from "./DLsite.ts";
import {
  genericCodeRe,
  makerLinkRe,
  quickCodeRe,
  workLinkRe,
} from "./lib/DLsite/mod.ts";
import type {
  Fumilink,
  MakerCodeGroups,
  MakerCodeLinkGroups,
  WorkCodeLinkGroups,
} from "./types/mod.ts";
import { html } from "./utils/html.ts";
import { replaceText } from "./utils/replaceText.ts";

function createLink(
  text: string,
  href: string,
  classList: string[],
  code: string,
): HTMLAnchorElement {
  const elem = html`<a
    class="${classList.join(" ")}"
    href="${href}"
    rel="noreferrer"
    target="_blank"
    data-code="${code}"
    >${text}</a
  >` as HTMLAnchorElement;
  return elem;
}

export function augumentDLsiteLinks(
  root: HTMLElement,
  addMentions = true,
): number {
  const links = root
    .querySelectorAll(`a[href*='dlsite.com']:not(.${C.LINK})`);
  let count = 0, match: RegExpExecArray | null;
  for (const link of links) {
    if ((match = workLinkRe.exec(link.href))) {
      const code = DLsiteWorkCode.get(match.groups as WorkCodeLinkGroups);
      link.dataset.code = `${code}`;
      link.classList.add(C.LINK, C.PREVIEWABLE, C.DLSITE_WORK);
      if (addMentions) code.addMention(link);
    } else if ((match = makerLinkRe.exec(link.href))) {
      const code = DLsiteMakerCode.get(match.groups as MakerCodeLinkGroups);
      link.dataset.code = `${code}`;
      link.classList.add(C.LINK, C.PREVIEWABLE, C.DLSITE_MAKER);
    } else continue;
    count++;
  }
  return count;
}

function linkifyDLsiteCode(match: RegExpExecArray): Fumilink {
  const code = DLsiteWorkCode.get(match.groups as WorkCodeLinkGroups);
  const link = createLink(
    match[0],
    code.getWorkUrl(),
    [C.LINK, C.PREVIEWABLE, C.DLSITE_WORK],
    `${code}`,
  ) as Fumilink;
  return link;
}

function linkifyDLsiteMakerCode(match: RegExpExecArray): Fumilink {
  const code = DLsiteMakerCode.get(match.groups as MakerCodeGroups);
  const link = createLink(
    match[0],
    code.getCreatorUrl(),
    [C.LINK, C.DLSITE_MAKER],
    `${code}`,
  ) as Fumilink;
  return link;
}

function linkifyDispatcher(match: RegExpExecArray): Fumilink {
  if (match.groups!.work_code) {
    return linkifyDLsiteCode(match);
  } else {
    return linkifyDLsiteMakerCode(match);
  }
}

function pruningFumiFilter(elem: Element) {
  return (
    elem.classList.contains(C.LINK) ||
    !quickCodeRe.test(elem.textContent!) ||
    /** Excludes hidden nodes. This method is really new, so also a failsafe
     * just to be sure. */
    !(elem.checkVisibility?.({ checkVisibilityCSS: true }) ?? true)
  );
}

/** Crawls the element's tree for DLsite codes and linkifies them. */
export function linkify(
  root: HTMLElement,
  { addMentions = true, branchPruning = false } = {},
): number {
  const links = branchPruning
    ? replaceText(root, genericCodeRe, linkifyDispatcher, {
      filter: pruningFumiFilter,
    })
    : replaceText(root, genericCodeRe, linkifyDispatcher, {
      classesToExclude: [C.LINK],
    });
  root.normalize();
  if (addMentions) {
    for (const link of links) {
      const code = DLsiteWorkCode.list.get(link.dataset.code);
      if (!code) continue;
      code.addMention(link);
    }
  }
  return links.length;
}
