import { C } from "../classNames.ts";
import { DLsiteWorkCode } from "./DLsite.ts";
import { Fumilink } from "./types/mod.ts";
import { html } from "./utils/html.ts";

let preview: HTMLImageElement;
let scheduledAnimationFrame = false;

/** Logic adapted from 4chanX. */
function calculateMargins(clientX: number, clientY: number) {
  const viewportHeight = document.documentElement.clientHeight;
  const viewportWidth = document.documentElement.clientWidth;
  const { height, width } = preview.getBoundingClientRect();
  const threshold = clientX <= viewportWidth / 2;
  const marginX = Math.min(
    (threshold ? clientX : viewportWidth - clientX) + 45,
    viewportWidth - width,
  );
  const top = Math.max(
    0,
    (clientY * (viewportHeight - height + 25)) / viewportHeight,
  );
  const [left, right] = threshold ? [marginX, null] : [null, marginX];
  return { top, left, right };
}

function positionPreview(clientX: number, clientY: number) {
  scheduledAnimationFrame = false;
  const { top, left, right } = calculateMargins(clientX, clientY);
  preview.style.top = top + "px";
  preview.style.left = left ? left + "px" : "";
  preview.style.right = right ? right + "px" : "";
}

function pointermoveHandler(event: PointerEvent) {
  if (scheduledAnimationFrame) return;
  const { clientX, clientY } = event;
  scheduledAnimationFrame = true;
  requestAnimationFrame(() => positionPreview(clientX, clientY));
}

function imageLoadHandler(target: Fumilink, event: PointerEvent) {
  const { clientX, clientY } = event;
  positionPreview(clientX, clientY);
  preview.classList.toggle(C.VISIBLE, true);
  target.addEventListener("pointermove", pointermoveHandler);
}

function unloadPreview(target: Fumilink, controller: AbortController) {
  controller.abort();
  preview.classList.toggle(C.VISIBLE, false);
  target.removeEventListener("pointermove", pointermoveHandler);
}

async function previewHoveredLink(
  target: Fumilink,
  event: PointerEvent,
): Promise<void> {
  const controller = new AbortController();
  const code = DLsiteWorkCode.list.get(target.dataset.code);
  if (!code) {
    throw new Error("Something happened.");
  }

  target.addEventListener(
    "pointerout",
    () => unloadPreview(target, controller),
    { once: true },
  );

  const image = await code.getMainImage();
  if (!image) return;

  if (!controller.signal.aborted) {
    preview.addEventListener("load", () => imageLoadHandler(target, event), {
      signal: controller.signal,
      once: true,
    });
    preview.src = image;
  }
}

function globalPointeroverHandler(event: PointerEvent) {
  const path = event.composedPath() as Element[];
  for (const target of path) {
    if (target.tagName !== "A") continue;
    if (target.classList.contains(C.PREVIEWABLE)) {
      previewHoveredLink(target as Fumilink, event);
      break;
    }
  }
}

/**
 * Initializes the preview element and adds a global pointerover event listener.
 */
export function initImagePreview(): void {
  preview = html`<img
    class="${C.PREVIEW}"
    referrerpolicy="no-referrer"
  /> ` as HTMLImageElement;
  document.body.append(preview);
  document.addEventListener("pointerover", globalPointeroverHandler);
}
