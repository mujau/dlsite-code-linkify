import { CodeList } from "./codeList.ts";
import { quickCodeRe } from "./lib/DLsite/code.ts";
import { augumentDLsiteLinks, linkify } from "./linkify.ts";

// deno-lint-ignore no-unused-vars
function nukeWbr(root: HTMLElement): void {
  root.querySelectorAll("wbr").forEach((elem) => {
    const parent = elem.parentElement!;
    parent.removeChild(elem);
    parent.normalize();
  });
}

function processPost(post: HTMLElement, addMentions = true): number {
  if (!quickCodeRe.test(post.textContent!)) return 0;

  let count = 0;
  const filename = post.querySelector(".fileText a");
  const body = post.querySelector<HTMLElement>(".postMessage")!;

  if (filename) {
    const trunc = filename.querySelector(".fnswitch");
    if (trunc) {
      const fntrunc = trunc.querySelector<HTMLElement>(".fntrunc")!;
      const fnfull = trunc.querySelector<HTMLElement>(".fnfull")!;
      count += linkify(fntrunc, { addMentions });
      count += linkify(fnfull, { addMentions: false });
    } else {
      count += linkify(filename, { addMentions });
    }
  }

  count += linkify(body, { addMentions });
  return count;
}

/** Server-rendered posts.*/
function scanRenderedPosts(root: HTMLElement): void {
  augumentDLsiteLinks(root);
  const posts = root.querySelectorAll<HTMLElement>(".postContainer");
  posts.forEach((post) => {
    processPost(post);
  });
}

function postObserver(mutations: MutationRecord[]) {
  for (const { target, addedNodes } of mutations) {
    let inline: boolean;
    if (!(target instanceof HTMLElement)) continue;
    if (target.classList.contains("thread")) {
      inline = false;
    } else if (target.classList.contains("postMessage")) {
      inline = true;
    } else {
      continue;
    }
    for (const node of addedNodes) {
      if (!(node instanceof HTMLElement)) continue;
      if (!inline) {
        if (!node.classList.contains("postContainer")) continue;
      } else if (!node.matches(".inline, .inlined")) continue;
      __DEV && console.debug("Processing new post: %o", node);
      let count = augumentDLsiteLinks(node, !inline);
      count += processPost(node, !inline);
      if (!inline && count > 0) CodeList.invalidate();
    }
  }
}

function hoverObserver(mutations: MutationRecord[]) {
  for (const { target, addedNodes } of mutations) {
    if (!(target instanceof HTMLElement)) continue;
    if (target.id !== "qp") continue;
    for (const node of addedNodes) {
      if (!(node instanceof HTMLElement)) continue;
      if (!node.classList.contains("postContainer")) continue;
      __DEV && console.debug("Processing hover preview: %o", node);
      augumentDLsiteLinks(node, false);
      processPost(node, false);
    }
  }
}

/** Client-rendered posts. */
function watchForNewPosts(root: HTMLElement): void {
  const observer = new MutationObserver(postObserver);
  observer.observe(root, { childList: true, subtree: true });
}

/** Entirely cosmetic. */
function watchHoverPreview(): void {
  const target = document.querySelector("#hoverUI");
  if (!target) return; // No support for native as it keeps recreating the container.
  const observer = new MutationObserver(hoverObserver);
  observer.observe(target, { childList: true, subtree: true });
}

export const yonchanDomains = [
  "boards.4chan.org",
  "4chan.gay",
] as const;

export function yonchan(hasCodes: boolean): void {
  const thread = document.querySelector<HTMLElement>(".thread");
  if (!thread) {
    throw new ReferenceError("Thread element not found");
  }
  if (hasCodes) {
    // Disabled for now to see if it even matters.
    // nukeWbr(thread);
    scanRenderedPosts(thread);
  }
  watchForNewPosts(thread);
  watchHoverPreview();
}
