export type Fumilink = {
  dataset: {
    code: string;
  };
} & HTMLAnchorElement;
