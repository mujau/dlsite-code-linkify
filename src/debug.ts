import { CodeList } from "./codeList.ts";
import { DLsiteWorkCode } from "./DLsite.ts";
import { WorkCode } from "./types/mod.ts";

function addDummyCodes() {
  const count = parseInt(
    prompt("How many codes should be added? 500 by default.", "500") ?? "0",
  );
  if (!count) return;
  const dummyAnchor = document.createElement("a");
  const start = Math.floor(Math.random() * (400000 - 100000) + 100000);
  for (let i = start; i < start + count; i++) {
    const code = DLsiteWorkCode.get({ work_code: `RJ${i}` as WorkCode });
    code.addMention(dummyAnchor);
  }
  CodeList.invalidate();
}

export function addDebugStuff(): void {
  GM.registerMenuCommand("Add dummy codes", addDummyCodes);
}
