/* v1.0.1 2024-09-08 */

export type ResponseWithOk<T> = VMScriptResponseObject<T> & { ok: boolean };

/** A promise based wrapper for `GM.xmlHttpRequest`, similar to native `fetch`. */
export function monkeyfetch<T = string>(
  details: VMScriptGMXHRDetails<T>,
  signal?: AbortSignal,
): Promise<ResponseWithOk<T>> {
  return new Promise((resolve, reject) => {
    Object.assign(details, {
      onerror: reject,
      onload: (response: VMScriptResponseObject<T>) => {
        const { status } = response;
        Object.assign(response, {
          ok: status >= 200 && status <= 299,
        });
        resolve(response as ResponseWithOk<T>);
      },
    });
    const control = GM.xmlHttpRequest(details);
    if (control.abort && signal instanceof AbortSignal) {
      signal.addEventListener("abort", () => control.abort(), { once: true });
    }
  });
}
