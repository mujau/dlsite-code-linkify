/* v2.0.0.0 2025-02-15 */

interface EventHandler {
  type: string;
  handler: (e: Event) => void;
}

export function html(
  strings: TemplateStringsArray,
  ...subs: unknown[]
): Element | DocumentFragment {
  const template = document.createElement("template");
  let rope = subs.length ? "" : strings[0]!;
  const eventHandlers: EventHandler[] = [];
  let eventHandlerIndex = 0;

  for (const [i, sub] of subs.entries()) {
    let handled = false;
    rope += strings[i]!.replace(/on:(.+) *= *$/, (match, type) => {
      if (!(typeof sub === "function")) {
        throw new TypeError(
          `Event listener must be followed by a function: ${match}\n${sub} is not a function`,
        );
      }
      handled = true;
      const handler = sub as (e: Event) => void;
      eventHandlers.push({ type, handler });
      return `__subref-eventhandler-${eventHandlerIndex++}`;
    });
    if (handled) continue;

    if (sub instanceof Node || sub instanceof Array) {
      rope += `<param __subref-node=${i}>`;
    } else {
      rope += String(sub);
    }
  }

  if (subs.length) {
    rope += strings[strings.length - 1];
  }

  template.innerHTML = rope;
  const slots = template.content.querySelectorAll("param[__subref-node]");

  for (const slot of slots) {
    const id = +slot.getAttribute("__subref-node")!;
    const sub = subs[id]!;
    if (sub instanceof Array) {
      slot.replaceWith(...sub);
    } else if (sub instanceof Node) {
      slot.replaceWith(sub);
    } else {
      throw new TypeError(`Unhandled substitution: ${sub}`);
    }
  }

  for (const [i, { type, handler }] of eventHandlers.entries()) {
    const attrName = `__subref-eventhandler-${i}`;
    const elem = template.content.querySelector(`[${attrName}]`)!;
    elem.removeAttribute(attrName);
    elem.addEventListener(type, handler);
  }

  return template.content.children.length > 1
    ? template.content
    : template.content.children[0]!;
}

export function collectRefs(root: ParentNode): Record<string, Element> {
  const refs: Record<string, Element> = {};
  root.querySelectorAll<Element & { ref: string }>("[ref]").forEach((e) => {
    refs[e.getAttribute("ref")!] = e;
    e.removeAttribute("ref");
  });
  return refs;
}
