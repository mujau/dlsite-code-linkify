export function assumeAs<T>(variable: unknown): asserts variable is T {}

export function assumeNonNullable<T>(
  variable: T,
): asserts variable is NonNullable<T> {}
