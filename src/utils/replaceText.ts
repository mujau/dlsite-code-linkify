/* v1.1.0 2024-04-11 */

function assumeAs<T>(v: unknown): asserts v is T {}

export interface Replacer {
  (execArray: RegExpExecArray): ChildNode;
}

export interface Filter {
  (elem: Element): boolean;
}

export interface ReplaceTextOptions {
  /** Elements with any HTML tag name in this array won't be traversed. */
  tagsToExclude?: string[];
  /** Elements with any class in this array won't be traversed. */
  classesToExclude?: string[];
  /** Elements for which this function returns `true` won't be traversed. */
  filter?: Filter;
}

/** Executes a regex search on a text node and replaces the matched range.
 * @returns the replacement in case of a match and `null` otherwise. */
export function replaceTextNode<R extends Replacer>(
  node: Text,
  regex: RegExp,
  replacer: R,
): ReturnType<R> | null {
  if (!node.data.trim()) return null;
  const match = regex.exec(node.data);
  regex.lastIndex = 0;
  if (!match) return null;
  const replacement = replacer(match) as ReturnType<R>;
  node = node.splitText(match.index);
  node.splitText(match[0].length);
  node.replaceWith(replacement);
  return replacement;
}

/** Executes a regex search on a document tree and replaces matched text
 * in-place.
 * @returns an array of replacements. */
export function replaceText<R extends Replacer>(
  root: ParentNode,
  regex: RegExp,
  replacer: R,
  { tagsToExclude, classesToExclude, filter }: ReplaceTextOptions = {},
): ReturnType<R>[] {
  const replacements: ReturnType<R>[] = [];
  const excludedTagSet =
    tagsToExclude?.reduce(
      (acc, val) => acc.add(val.toUpperCase()),
      new Set<string>(),
    ) ?? null;
  const selector = classesToExclude ? "." + classesToExclude.join(", .") : null;

  const reject = (node: Element) =>
    !!(
      excludedTagSet?.has(node.tagName) ||
      (selector && node.matches(selector)) ||
      filter?.(node)
    );

  const stack = [root.firstChild];

  while (stack.length) {
    let node = stack.pop();
    while (node) {
      switch (node.nodeType) {
        case Node.ELEMENT_NODE: {
          assumeAs<Element>(node);
          if (!reject(node)) {
            if (node.nextSibling) stack.push(node.nextSibling);
            node = node.firstChild;
            continue;
          }
          break;
        }
        case Node.TEXT_NODE: {
          assumeAs<Text>(node);
          const replacement = replaceTextNode(node, regex, replacer);
          if (replacement) {
            replacements.push(replacement);
            node = replacement;
          }
          break;
        }
      }
      node = node.nextSibling;
    }
  }
  return replacements;
}
