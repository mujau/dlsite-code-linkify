/** Comparator function for sorting nodes in document position order. */
export function nodePositionComparator(a: Node, b: Node): -1 | 0 | 1 {
  const pos = a.compareDocumentPosition(b);
  if (pos & Node.DOCUMENT_POSITION_PRECEDING) return 1;
  if (pos & Node.DOCUMENT_POSITION_FOLLOWING) return -1;
  else return 0;
}

/** Inserts a node into an array of nodes at its relative document position. The
 * array isn't sorted, so the order isn't guaranteed in case of node movement.
 * */
export function sortedNodeInsertion(arr: Node[], node: Node): void {
  let i = arr.length;
  if (i === 0) {
    arr.push(node);
  } else {
    do {
      i--;
      if (nodePositionComparator(node, arr[i]!) === 1) {
        arr.splice(i + 1, 0, node);
        break;
      }
      if (i === 0) {
        arr.unshift(node);
      }
    } while (i > 0);
  }
}
