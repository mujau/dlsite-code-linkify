import { SCRIPT_NAME } from "../constants.ts";
import { namespacedKebabFromLabels } from "./cssUtils.ts";

export const C = (() => {
  const labels = [
    "PREVIEW",
    "LINK",
    "LIST",
    "LIST_ITEM",
    "LIST_HIDER",
    "LIST_TOTAL",
    "VISIBLE",
    "FOCUSED",
    "INVALID",
    "PREVIEWABLE",
    "DLSITE_WORK",
    "DLSITE_MAKER",
  ] as const;
  return namespacedKebabFromLabels(labels, `${SCRIPT_NAME}--`);
})();

export type ClassNames = typeof C;
