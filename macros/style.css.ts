import { css } from "./cssUtils.ts";
import { C } from "./classNames.ts";

export default css`
.${C.PREVIEW} {
  visibility: hidden;
  position: fixed;
  pointer-events: none;
  z-index: 100;

  &.${C.VISIBLE} {
    visibility: visible;
  }
}

.${C.LIST} {
  box-sizing: border-box;
  position: fixed;
  display: grid;
  max-width: 60%;
  max-height: 400px;
  right: 10px;
  top: 40px;
  padding: 5px;
  column-gap: 10px;
  grid-auto-flow: column;
  grid-template-rows: repeat(auto-fit, 20px);
  border: 1px solid #b7c5d9;
  background-color: #d6daf0;
  z-index: 10;
  overflow-x: auto;

  a.${C.LIST_ITEM} {
    width: max-content;
    cursor: pointer;
    color: #34345c !important;
    text-decoration: none;
  }

  a.${C.LIST_ITEM}:hover {
    color: #d00 !important;
  }

  .${C.LIST_HIDER} {
    justify-self: center;
  }

  .${C.LIST_TOTAL} {
     color: #000;
  }
}

/* Needs custom styling because \`.focus()\` can't force focus-ring to appear.
 * It sometimes bugs out in chromium, but whatever. */
a.${C.LINK} {
  &.${C.FOCUSED} {
    outline-style: auto !important;
  }

  &.${C.INVALID} {
    text-decoration: line-through !important;
  }
}
`;
