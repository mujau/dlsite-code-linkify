/* v1.0.0.0 2025-02-17 */

// deno-lint-ignore-file no-explicit-any
import type { KebabCase, Simplify } from "npm:type-fest@4.35.0";

/** From https://github.com/microsoft/TypeScript/issues/58779 - maybe it will start working one day */
type GenericReturnType<F extends (arg: any) => any, P> = F extends
  (arg: P) => infer R ? R : never;

export type NamespacedKebabFromLabels<
  L extends readonly string[],
  NS extends string,
> = Simplify<
  {
    readonly [K in keyof L & `${number}` as L[K]]: `${NS}${KebabCase<L[K]>}`;
  }
>;

function screamingToKebab<T extends string>(str: T): KebabCase<T> {
  return str.toLowerCase().replaceAll("_", "-") as KebabCase<T>;
}

/** Bad mapper results because https://github.com/microsoft/TypeScript/issues/40179 */
export function mapToObject<
  A extends readonly string[],
  M extends <V extends string>(v: V) => R,
  R extends string,
>(arr: A, mapper: M): {
  [K in keyof A & `${number}` as A[K]]: GenericReturnType<M, A[K]>;
} {
  const mapped: Record<string, string> = Object.create(null);
  for (const v of arr) {
    mapped[v] = mapper(v);
  }
  return mapped as any;
}

export function namespacedKebabFromLabels<
  L extends readonly string[],
  NS extends string,
>(labels: L, ns: NS): NamespacedKebabFromLabels<L, NS> {
  return mapToObject(
    labels,
    (v) => `${ns}${screamingToKebab(v)}` as const,
  ) as any;
}

export const css = (
  strings: TemplateStringsArray,
  ...values: string[]
): string => String.raw(strings, ...values);
