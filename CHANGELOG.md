# vnext

- Restored highlighting of codes in posts from hovered cross-thread quotelinks. Purely cosmetic, but not doing that looks broken.
- Changed all CSS classes used by the script. Shouldn't matter for you at all unless were targeting them for whatever reason.

# v1.3.0 (2025-02-16)

- Fixed code list's counter counting codes from posts in other threads that were loaded inline.
- Added new supported domains.
- Other internal changes.
- Replaced uhtml dependency with a custom html rendering solution.
- Switched build system to deno + esbuild because node randomly stopped working. The diff of the compiled script is pretty big as a result.

# v1.2.4 (2024-02-11)

- Invalid codes are now styled with strikethrough.
- Network errors shouldn't break preview anymore.
- Fixed regexes to stop matching codes of wrong length yet again.

# v1.2.3 (2024-02-03)

- Fixed image preview race condition.

# v1.2.2 (2024-01-16)

- Various other fixes.
- Improved image preview's fallback behavior.
- Fixed code list order.

# v1.2.1 (2023-12-26)

- Accidentally made the generic path so damn fast the b4k dedicated one is pointless now.
- Fixed codes from inlined posts being counted for the code list.
- Fixed image preview some more.

# v1.2.0 (2023-12-23)

- The script is now compiled to iife.
- Cross-thread embeds are now properly linkified.
- Added fumicon.
- 100% faster generic path. Not much worse than dedicated paths now.
- Many internal changes. 40% faster page load on 4chan.
- Fixed preview images lingering after quickly moving the pointer over a code.
- Clicking code list codes with the middle mouse button now opens their DLsite page.
- Removed 4channel and arca.live from supported sites. Added panda.

# v1.1.0 (2023-07-18)

- Added code list (can be toggled from Violentmonkey's menu).
- Improved code handling.
- Removed nyaa.si and anime-sharing from supported sites.
- Removed debug logging from normal builds.
- Removed source mapping.
- Formatted the compiled script with prettier.
