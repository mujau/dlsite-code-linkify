# DLsite Code Linkify

DLsite code linkification and preview for 4chan and more.\
Rarely, announce stage works end up linkified to 404 pages. Use together with [dlsite-tweaks](https://gitlab.com/mujau/dlsite-tweaks) to automatically redirect them.

### Supported sites:

- 4chan (including the gay flavor)
- b4k archive (images only with [pee-companion](https://git.coom.tech/fuckjannies/lolipiss/src/commit/80a96cf64a68386304a883014d66d8dbcb9ba6bf) or some other CSP disabling extension, due to restrictive CSP rules)
- archived.moe
- GGn
- e-hentai/exhentai

## Instalation

The script is tested in the latest stable chromium with [violentmonkey](https://violentmonkey.github.io/).

To install, load `./dist/dlsite-code-linkify.user.js` ([click](https://gitlab.com/mujau/dlsite-code-linkify/-/raw/master/dist/dlsite-code-linkify.user.js)) in your userscript manager.

## Building

The project is managed with ~~pnpm~~ [deno](https://deno.com/).
- Run `deno i` to install dependencies.
- Run `deno task build` to build.
