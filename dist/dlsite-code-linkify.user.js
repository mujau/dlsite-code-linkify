// ==UserScript==
// @name        DLsite Code Linkify
// @namespace   green-is-my-paddy
// @version     1.3.0
// @description Linkifies DLsite work and creator codes.
// @author      mujau
// @homepageURL https://gitlab.com/mujau/dlsite-code-linkify
// @supportURL  https://gitlab.com/mujau/dlsite-code-linkify/-/issues
// @downloadURL https://gitlab.com/mujau/dlsite-code-linkify/-/raw/master/dist/dlsite-code-linkify.user.js
// @icon        https://gitlab.com/mujau/dlsite-code-linkify/-/raw/1.2.0/img/fumicon.png
// @grant       GM.xmlHttpRequest
// @grant       GM.registerMenuCommand
// @grant       GM.addStyle
// @run-at      document-end
// @match       *://boards.4chan.org/*
// @match       *://4chan.gay/*
// @match       *://arch.b4k.co/*
// @match       *://arch.b4k.dev/*
// @match       *://archived.moe/*
// @match       *://gazellegames.net/*
// @match       *://e-hentai.org/g/*
// @match       *://exhentai.org/g/*
// ==/UserScript==

"use strict";

// src/lib/DLsite/utils.ts
function mergeRegex(parts, flags = "") {
  const patternList = [];
  const flagList = [];
  for (const part of parts) {
    if (part instanceof RegExp) {
      patternList.push(part.source);
      flagList.push(part.flags);
    } else patternList.push(part);
  }
  const mergedFlags = Array.from(
    flagList.reduce(
      (acc, flags2) => {
        flags2.split("").forEach((f) => acc.add(f));
        return acc;
      },
      new Set(flags.split(""))
    )
  ).join("");
  return new RegExp(patternList.join(""), mergedFlags);
}
function splitAt(str, index) {
  return [str.slice(0, index), str.slice(index)];
}

// src/lib/DLsite/code.ts
var quickCodeRe = /[RBV][JGE]\d{4,8}/i;
var workLinkPrefixRe = /(?:https?:\/\/)?(?:www\.)?dlsite.com\/(?<site>[^\/]+)\/(?<ana>work|announce)\/=\/product_id\//i;
var workLinkPostfixRe = /(?:\.html)?/i;
var workCodeRe = /(?<work_code>(?:RJ|RE|VJ|BJ)(?:\d{8}|\d{6}|\d{4}))(?!\d)/i;
var creatorCodeRe = /(?<creator_code>(?:RG|VG|BG)(?:\d{8}|\d{4,5}))(?!\d)/i;
var workLinkRe = mergeRegex([
  workLinkPrefixRe,
  workCodeRe,
  workLinkPostfixRe
]);
var genericCodeRe = mergeRegex([
  "(?:",
  workLinkPrefixRe,
  ")?",
  workCodeRe,
  workLinkPostfixRe,
  "|",
  creatorCodeRe
]);
function guessSite(code) {
  const [prefix] = splitAt(code, 2);
  switch (prefix) {
    case "RJ":
    case "RG":
      return "maniax";
    case "RE":
      return "ecchi-eng";
    case "VJ":
    case "VG":
      return "pro";
    case "BJ":
    case "BG":
      return "books";
    default: {
      console.warn(`Unknown code prefix: \`${prefix}\``);
      return "home";
    }
  }
}
function getImagePath(prefix) {
  switch (prefix) {
    case "RJ":
    case "RE":
      return "doujin";
    case "VJ":
      return "professional";
    case "BJ":
      return "books";
    default: {
      throw new Error(`Unknown code prefix: \`${prefix}\``);
    }
  }
}
function getBucket(prefix, no) {
  const [a, b] = splitAt(no, -3);
  const bucket = `${+a + (b === "000" ? 0 : 1)}`.padStart(a.length, "0");
  return `${prefix}${bucket}000`;
}
function getMainImageUrl(code, announce = false) {
  const [prefix, no] = splitAt(code, 2);
  const path = getImagePath(prefix);
  const bucket = getBucket(prefix, no);
  return announce ? `https://img.dlsite.jp/modpub/images2/ana/${path}/${bucket}/${code}_ana_img_main.jpg` : `https://img.dlsite.jp/modpub/images2/work/${path}/${bucket}/${code}_img_main.jpg`;
}
function getWorkUrl(code, site = guessSite(code), announce = false) {
  return announce ? `https://www.dlsite.com/${site}/announce/=/product_id/${code}.html` : `https://www.dlsite.com/${site}/work/=/product_id/${code}.html`;
}
function getCreatorUrl(code, site = guessSite(code)) {
  return `https://www.dlsite.com/${site}/circle/profile/=/maker_id/${code}.html`;
}
function normalizeWorkCode(code) {
  const [prefix, no] = splitAt(code, 2);
  return prefix.toUpperCase() + no.padStart(6, "0");
}
function normalizeCreatorCode(code) {
  const [prefix, no] = splitAt(code, 2);
  return prefix.toUpperCase() + no.padStart(5, "0");
}

// src/utils/defer.ts
function defer() {
  let resolve;
  let reject;
  const promise = new Promise((_resolve, _reject) => {
    resolve = _resolve;
    reject = _reject;
  });
  return { resolve, reject, promise };
}

// src/utils/monkeyfetch.ts
function monkeyfetch(details, signal) {
  return new Promise((resolve, reject) => {
    Object.assign(details, {
      onerror: reject,
      onload: (response) => {
        const { status } = response;
        Object.assign(response, {
          ok: status >= 200 && status <= 299
        });
        resolve(response);
      }
    });
    const control = GM.xmlHttpRequest(details);
    if (control.abort && signal instanceof AbortSignal) {
      signal.addEventListener("abort", () => control.abort(), { once: true });
    }
  });
}

// src/utils/various.ts
function nodePositionComparator(a, b) {
  const pos = a.compareDocumentPosition(b);
  if (pos & Node.DOCUMENT_POSITION_PRECEDING) return 1;
  if (pos & Node.DOCUMENT_POSITION_FOLLOWING) return -1;
  else return 0;
}
function sortedNodeInsertion(arr, node) {
  let i = arr.length;
  if (i === 0) {
    arr.push(node);
  } else {
    do {
      i--;
      if (nodePositionComparator(node, arr[i]) === 1) {
        arr.splice(i + 1, 0, node);
        break;
      }
      if (i === 0) {
        arr.unshift(node);
      }
    } while (i > 0);
  }
}

// src/DLsite.ts
async function imageFetch(url) {
  const headers = { "X-Requested-With": "XMLHttpRequest" };
  return await monkeyfetch({
    headers,
    method: "GET",
    responseType: "blob",
    url,
    anonymous: true
  });
}
var DLsiteCode = class {
  invalid = false;
  toString() {
    return this.code;
  }
  [Symbol.toPrimitive](hint) {
    return hint === "number" ? +this.code.slice(2) : this.code;
  }
  getFumilinks() {
    return document.querySelectorAll(`a.fumilink[data-code=${this}]`);
  }
};
var DLsiteWorkCode = class _DLsiteWorkCode extends DLsiteCode {
  static list = /* @__PURE__ */ new Map();
  static uniqueMentionCount = 0;
  static totalMentionCount = 0;
  code;
  site;
  announce;
  #image_main = null;
  mentions = [];
  getMainImageUrl() {
    const { code, announce } = this;
    return getMainImageUrl(code, announce);
  }
  getWorkUrl() {
    const { code, site, announce } = this;
    return getWorkUrl(code, site, announce);
  }
  #updateLinks() {
    console.debug("Updating links");
    const links = this.getFumilinks();
    const workUrl = this.getWorkUrl();
    links.forEach((elem) => elem.href = workUrl);
  }
  #markInvalid() {
    console.debug("Marking the code as invalid");
    const links = this.getFumilinks();
    this.invalid = true;
    links.forEach(
      (elem) => elem.classList.replace("fumi__previewable", "fumi__invalid")
    );
  }
  /** Tries to fetch main image at both work and announce endpoints and marks
   * the code as invalid if neither works. */
  async getMainImage() {
    if (this.invalid) {
      console.error(this);
      throw new Error("Attempted to fetch main image of an invalid code");
    }
    if (this.#image_main) {
      if (this.#image_main instanceof Promise) {
        console.debug(`${this} has a pending main image fetch: %o`, this);
      } else {
        console.debug(`Main image of ${this} was already fetched: %o`, this);
      }
      return this.#image_main;
    }
    console.debug(`Fetching main image of ${this}: %o`, this);
    const { promise, resolve } = defer();
    this.#image_main = promise;
    try {
      for (let i = 1; i <= 2; i++) {
        const { response, status, ok } = await imageFetch(
          this.getMainImageUrl()
        );
        if (!ok || !response) {
          console.debug(
            `Main image fetch failure status: ${status}
${this.getMainImageUrl()} couldn't be fetched`
          );
          this.announce = !this.announce;
          if (i >= 2) this.#markInvalid();
        } else {
          console.debug(`Succeeded after ${i === 1 ? "1 try" : `${i} tries`}`);
          if (i % 2 === 0) this.#updateLinks();
          this.#image_main = URL.createObjectURL(response);
          break;
        }
      }
    } catch (err) {
      console.error("Main image fetch error: %o", err);
    }
    if (this.#image_main instanceof Promise) {
      this.#image_main = null;
    }
    resolve(this.#image_main);
    return this.#image_main;
  }
  addMention(mention) {
    if (!this.mentions.length) {
      _DLsiteWorkCode.uniqueMentionCount++;
    }
    sortedNodeInsertion(this.mentions, mention);
    _DLsiteWorkCode.totalMentionCount++;
  }
  constructor(code, site, ana) {
    super();
    if (ana) {
      this.announce = ana === "announce";
    }
    this.code = code;
    this.site = site ?? guessSite(code);
    _DLsiteWorkCode.list.set(code, this);
  }
  static get(data) {
    const { site, ana, work_code } = data;
    if (!work_code) {
      throw new Error("No code");
    }
    const code = normalizeWorkCode(work_code);
    const existing = _DLsiteWorkCode.list.get(code);
    if (existing) {
      if (ana && existing.announce === void 0) {
        existing.site = site;
        existing.announce = ana === "announce";
        const workUrl = existing.getWorkUrl();
        const links = existing.getFumilinks();
        links.forEach((elem) => elem.href = workUrl);
      }
      return existing;
    } else return new _DLsiteWorkCode(code, site, ana);
  }
};
var DLsiteCreatorCode = class _DLsiteCreatorCode extends DLsiteCode {
  static list = /* @__PURE__ */ new Map();
  code;
  site;
  icon;
  getCreatorUrl() {
    const { code, site } = this;
    return getCreatorUrl(code, site);
  }
  constructor(code) {
    super();
    this.code = code;
    this.site = guessSite(this.code);
    _DLsiteCreatorCode.list.set(code, this);
  }
  static get(data) {
    const { creator_code } = data;
    if (!creator_code) {
      throw new Error("No code");
    }
    const code = normalizeCreatorCode(creator_code);
    const existing = _DLsiteCreatorCode.list.get(code);
    if (existing) return existing;
    else return new _DLsiteCreatorCode(code);
  }
};

// src/utils/html.ts
function html(strings, ...subs) {
  const template = document.createElement("template");
  let rope = subs.length ? "" : strings[0];
  const eventHandlers = [];
  let eventHandlerIndex = 0;
  for (const [i, sub] of subs.entries()) {
    let handled = false;
    rope += strings[i].replace(/on:(.+) *= *$/, (match, type) => {
      if (!(typeof sub === "function")) {
        throw new TypeError(
          `Event listener must be followed by a function: ${match}
${sub} is not a function`
        );
      }
      handled = true;
      const handler = sub;
      eventHandlers.push({ type, handler });
      return `__subref-eventhandler-${eventHandlerIndex++}`;
    });
    if (handled) continue;
    if (sub instanceof Node || sub instanceof Array) {
      rope += `<param __subref-node=${i}>`;
    } else {
      rope += String(sub);
    }
  }
  if (subs.length) {
    rope += strings[strings.length - 1];
  }
  template.innerHTML = rope;
  const slots = template.content.querySelectorAll("param[__subref-node]");
  for (const slot of slots) {
    const id = +slot.getAttribute("__subref-node");
    const sub = subs[id];
    if (sub instanceof Array) {
      slot.replaceWith(...sub);
    } else if (sub instanceof Node) {
      slot.replaceWith(sub);
    } else {
      throw new TypeError(`Unhandled substitution: ${sub}`);
    }
  }
  for (const [i, { type, handler }] of eventHandlers.entries()) {
    const attrName = `__subref-eventhandler-${i}`;
    const elem = template.content.querySelector(`[${attrName}]`);
    elem.removeAttribute(attrName);
    elem.addEventListener(type, handler);
  }
  return template.content.children.length > 1 ? template.content : template.content.children[0];
}

// src/codeList.ts
var root;
var enabled = false;
var currentCode;
var mentionIndex = 0;
var scheduledAnimationFrame = false;
var list = [];
var lastSize = 0;
function onMouseUp(event, code) {
  if (event.button !== 0) return;
  if (currentCode !== code) {
    currentCode = code;
    mentionIndex = 0;
  } else {
    mentionIndex = mentionIndex < code.mentions.length - 1 ? mentionIndex + 1 : 0;
  }
  const mention = code.mentions[mentionIndex];
  console.debug("Focusing mention: %o", mention);
  mention.focus();
  mention.classList.toggle("fumi__focused", true);
  mention.addEventListener(
    "focusout",
    () => mention.classList.toggle("fumi__focused", false),
    { once: true }
  );
}
function getClasses({ invalid }) {
  return `fumilink ${!invalid ? "fumi__previewable" : "fumi__invalid"} dlsite-work fumilist__item`;
}
function getBody({ code, mentions }) {
  return `${code}${mentions.length > 1 ? ` (${mentions.length})` : ""}`;
}
function getOrderedList() {
  return Array.from(DLsiteWorkCode.list.values()).filter((a) => a.mentions.length > 0).sort((a, b) => nodePositionComparator(a.mentions[0], b.mentions[0]));
}
function toggle() {
  if (!enabled) {
    root = html`<div class="fumilist"></div>`;
    document.body.append(root);
    renderList();
  } else {
    root.remove();
  }
  enabled = !enabled;
}
function renderList() {
  performance.mark("renderCodeListStart");
  const unique = DLsiteWorkCode.uniqueMentionCount;
  const total = DLsiteWorkCode.totalMentionCount;
  const title = `${unique} unique codes (${total} in total)`;
  if (unique > lastSize) {
    list = getOrderedList();
    lastSize = unique;
  }
  root.replaceChildren(
    html`
      <a class="fumilist__item fumilist__hider" on:click=${toggle}>[hide]</a>
      ${list.map((code) => {
      return html`<a
          class="${getClasses(code)}"
          href="${code.getWorkUrl()}"
          rel="noreferrer"
          target="_blank"
          data-code="${code}"
          on:mousedown=${(event) => event.preventDefault()}
          on:click=${(event) => event.preventDefault()}
          on:mouseup=${(event) => onMouseUp(event, code)}
          >${getBody(code)}</a
        >`;
    })}
      <span class="fumilist__total" title="${title}">(${unique}/${total})</span>
    `
  );
  const m = performance.measure("renderCodeList", "renderCodeListStart");
  console.debug(`Rendering code list took ${m.duration}ms`);
}
var CodeList = {
  toggle,
  invalidate() {
    if (scheduledAnimationFrame || !enabled) {
      return;
    }
    scheduledAnimationFrame = true;
    requestAnimationFrame(() => {
      renderList();
      scheduledAnimationFrame = false;
    });
  },
  registerMenu() {
    GM.registerMenuCommand("Toggle code list", CodeList.toggle);
  }
};

// src/fumiPreview.ts
var preview;
var scheduledAnimationFrame2 = false;
function calculateMargins(clientX, clientY) {
  const viewportHeight = document.documentElement.clientHeight;
  const viewportWidth = document.documentElement.clientWidth;
  const { height, width } = preview.getBoundingClientRect();
  const threshold = clientX <= viewportWidth / 2;
  const marginX = Math.min(
    (threshold ? clientX : viewportWidth - clientX) + 45,
    viewportWidth - width
  );
  const top = Math.max(
    0,
    clientY * (viewportHeight - height + 25) / viewportHeight
  );
  const [left, right] = threshold ? [marginX, null] : [null, marginX];
  return { top, left, right };
}
function positionPreview(clientX, clientY) {
  scheduledAnimationFrame2 = false;
  const { top, left, right } = calculateMargins(clientX, clientY);
  preview.style.top = top + "px";
  preview.style.left = left ? left + "px" : "";
  preview.style.right = right ? right + "px" : "";
}
function pointermoveHandler(event) {
  if (scheduledAnimationFrame2) return;
  const { clientX, clientY } = event;
  scheduledAnimationFrame2 = true;
  requestAnimationFrame(() => positionPreview(clientX, clientY));
}
function imageLoadHandler(target, event) {
  const { clientX, clientY } = event;
  positionPreview(clientX, clientY);
  preview.classList.toggle("visible", true);
  target.addEventListener("pointermove", pointermoveHandler);
}
function unloadPreview(target, controller) {
  controller.abort();
  preview.classList.toggle("visible", false);
  target.removeEventListener("pointermove", pointermoveHandler);
}
async function previewHoveredLink(target, event) {
  const controller = new AbortController();
  const code = DLsiteWorkCode.list.get(target.dataset.code);
  if (!code) {
    throw new Error("Something happened.");
  }
  target.addEventListener(
    "pointerout",
    () => unloadPreview(target, controller),
    { once: true }
  );
  const image = await code.getMainImage();
  if (!image) return;
  if (!controller.signal.aborted) {
    preview.addEventListener("load", () => imageLoadHandler(target, event), {
      signal: controller.signal,
      once: true
    });
    preview.src = image;
  }
}
function globalPointeroverHandler(event) {
  const path = event.composedPath();
  for (const target of path) {
    if (target.tagName !== "A") continue;
    if (target.classList.contains("fumi__previewable")) {
      previewHoveredLink(target, event);
      break;
    }
  }
}
function initImagePreview() {
  preview = html`<img
    class="fumipreview"
    referrerpolicy="no-referrer"
  /> `;
  document.body.append(preview);
  document.addEventListener("pointerover", globalPointeroverHandler);
}

// src/utils/replaceText.ts
function assumeAs(v) {
}
function replaceTextNode(node, regex, replacer) {
  if (!node.data.trim()) return null;
  const match = regex.exec(node.data);
  regex.lastIndex = 0;
  if (!match) return null;
  const replacement = replacer(match);
  node = node.splitText(match.index);
  node.splitText(match[0].length);
  node.replaceWith(replacement);
  return replacement;
}
function replaceText(root2, regex, replacer, { tagsToExclude, classesToExclude, filter } = {}) {
  const replacements = [];
  const excludedTagSet = tagsToExclude?.reduce(
    (acc, val) => acc.add(val.toUpperCase()),
    /* @__PURE__ */ new Set()
  ) ?? null;
  const selector = classesToExclude ? "." + classesToExclude.join(", .") : null;
  const reject = (node) => !!(excludedTagSet?.has(node.tagName) || selector && node.matches(selector) || filter?.(node));
  const stack = [root2.firstChild];
  while (stack.length) {
    let node = stack.pop();
    while (node) {
      switch (node.nodeType) {
        case Node.ELEMENT_NODE: {
          assumeAs(node);
          if (!reject(node)) {
            if (node.nextSibling) stack.push(node.nextSibling);
            node = node.firstChild;
            continue;
          }
          break;
        }
        case Node.TEXT_NODE: {
          assumeAs(node);
          const replacement = replaceTextNode(node, regex, replacer);
          if (replacement) {
            replacements.push(replacement);
            node = replacement;
          }
          break;
        }
      }
      node = node.nextSibling;
    }
  }
  return replacements;
}

// src/linkify.ts
function createLink(text, href, classList, code) {
  const elem = html`<a
    class="${classList.join(" ")}"
    href="${href}"
    rel="noreferrer"
    target="_blank"
    data-code="${code}"
    >${text}</a
  >`;
  return elem;
}
function augumentDLsiteWorkLinks(root2, addMentions = true) {
  const links = root2.querySelectorAll("a[href*='dlsite.com']:not(.fumilink)");
  let count = 0;
  for (const link of links) {
    const match = workLinkRe.exec(link.href);
    if (!match) continue;
    count++;
    const code = DLsiteWorkCode.get(match.groups);
    link.dataset.code = `${code}`;
    link.classList.add("fumilink", "fumi__previewable", "dlsite-work");
    if (addMentions) code.addMention(link);
  }
  return count;
}
function linkifyDLsiteCode(match) {
  const code = DLsiteWorkCode.get(match.groups);
  const link = createLink(
    match[0],
    code.getWorkUrl(),
    ["fumilink", "fumi__previewable", "dlsite-work"],
    `${code}`
  );
  return link;
}
function linkifyDLsiteCreatorCode(match) {
  const code = DLsiteCreatorCode.get(match.groups);
  const link = createLink(
    match[0],
    code.getCreatorUrl(),
    ["fumilink", "dlsite-creator"],
    `${code}`
  );
  return link;
}
function linkifyDispatcher(match) {
  if (match.groups.work_code) {
    return linkifyDLsiteCode(match);
  } else {
    return linkifyDLsiteCreatorCode(match);
  }
}
function pruningFumiFilter(elem) {
  return elem.classList.contains("fumilink") || !quickCodeRe.test(elem.textContent) || /** Excludes hidden nodes. This method is really new, so also a failsafe
   * just to be sure. */
  !(elem.checkVisibility?.({ checkVisibilityCSS: true }) ?? true);
}
function linkify(root2, { addMentions = true, branchPruning = false } = {}) {
  const links = branchPruning ? replaceText(root2, genericCodeRe, linkifyDispatcher, {
    filter: pruningFumiFilter
  }) : replaceText(root2, genericCodeRe, linkifyDispatcher, {
    classesToExclude: ["fumilink"]
  });
  root2.normalize();
  if (addMentions) {
    for (const link of links) {
      const code = DLsiteWorkCode.list.get(link.dataset.code);
      if (!code) continue;
      code.addMention(link);
    }
  }
  return links.length;
}

// src/yonchan.ts
function nukeWbr(root2) {
  root2.querySelectorAll("wbr").forEach((elem) => {
    const parent = elem.parentElement;
    parent.removeChild(elem);
    parent.normalize();
  });
}
function processPost(post, addMentions = true) {
  if (!quickCodeRe.test(post.textContent)) return 0;
  let count = 0;
  const filename = post.querySelector(".fileText a");
  const body = post.querySelector(".postMessage");
  if (filename) {
    const trunc = filename.querySelector(".fnswitch");
    if (trunc) {
      const fntrunc = trunc.querySelector(".fntrunc");
      const fnfull = trunc.querySelector(".fnfull");
      count += linkify(fntrunc, { addMentions });
      count += linkify(fnfull, { addMentions: false });
    } else {
      count += linkify(filename, { addMentions });
    }
  }
  count += linkify(body, { addMentions });
  return count;
}
function scanRenderedPosts(root2) {
  augumentDLsiteWorkLinks(root2);
  const posts = root2.querySelectorAll(".postContainer");
  posts.forEach((post) => {
    processPost(post);
  });
}
function postObserver(mutations) {
  for (const { target, addedNodes } of mutations) {
    let inline;
    if (!(target instanceof HTMLElement)) continue;
    if (target.classList.contains("thread")) {
      inline = false;
    } else if (target.classList.contains("postMessage")) {
      inline = true;
    } else {
      continue;
    }
    for (const node of addedNodes) {
      if (!(node instanceof HTMLElement)) continue;
      if (!inline) {
        if (!node.classList.contains("postContainer")) continue;
      } else if (!node.matches(".inline, .inlined")) continue;
      let count = augumentDLsiteWorkLinks(node, !inline);
      count += processPost(node, !inline);
      if (!inline && count > 0) CodeList.invalidate();
    }
  }
}
function watchForNewPosts(root2) {
  const observer = new MutationObserver(postObserver);
  observer.observe(root2, { childList: true, subtree: true });
}
var yonchanDomains = [
  "boards.4chan.org",
  "4chan.gay"
];
function yonchan(hasCodes) {
  const thread = document.querySelector(".thread");
  if (!thread) {
    throw new ReferenceError("Thread element not found");
  }
  if (hasCodes) {
    nukeWbr(thread);
    scanRenderedPosts(thread);
  }
  watchForNewPosts(thread);
}

// src/main.ts
function generic() {
  augumentDLsiteWorkLinks(document.body);
  linkify(document.body, { branchPruning: true });
}
function router() {
  const hasCodes = quickCodeRe.test(document.body.textContent);
  console.debug(`The page has codes: ${hasCodes}`);
  if (yonchanDomains.includes(location.hostname)) {
    yonchan(hasCodes);
  } else if (hasCodes) {
    generic();
  } else {
    console.debug("Nothing to linkify");
    return false;
  }
  return true;
}
function init() {
  console.debug("dlsite-code-linkify init has started");
  const t1 = performance.now();
  const matched = router();
  const t2 = performance.now();
  console.debug(`Linkification took ${t2 - t1}ms`);
  if (!matched) return;
  GM.addStyle(".fumipreview{visibility:hidden;position:fixed;pointer-events:none;z-index:100}.fumipreview.visible{visibility:visible}.fumilist{box-sizing:border-box;position:fixed;display:grid;max-width:60%;max-height:400px;right:10px;top:40px;padding:5px;column-gap:10px;grid-auto-flow:column;grid-template-rows:repeat(auto-fit,20px);border:1px solid #b7c5d9;background-color:#d6daf0;z-index:10;overflow-x:auto}.fumilist a.fumilist__item{width:max-content;cursor:pointer;color:#34345c!important;text-decoration:none}.fumilist a.fumilist__item:hover{color:#d00!important}.fumilist .fumilist__hider{justify-self:center}.fumilist .fumilist__total{color:#000}a.fumilink.fumi__focused{outline-style:auto!important}.fumilink.fumi__invalid{text-decoration:line-through!important}");
  initImagePreview();
  CodeList.registerMenu();
}
requestIdleCallback(init);
