import type { ClassNames } from "./macros/classNames.ts";

export const C = JSON.parse(__CLASS_NAMES) as ClassNames;
