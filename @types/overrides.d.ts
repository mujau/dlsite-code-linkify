/* v1.0.0.0 2025-02-11 */

/**
 * @see https://github.com/microsoft/TypeScript/issues/14520 (Enable type parameter lower-bound syntax)
 */
interface Set<T> {
  has(value: unknown): value is T;
}

/**
 * @see https://github.com/microsoft/TypeScript/issues/14520 (Enable type parameter lower-bound syntax)
 */
interface Array<T> {
  includes(searchElement: unknown, fromIndex?: number): searchElement is T;
}

/**
 * @see https://github.com/microsoft/TypeScript/issues/14520 (Enable type parameter lower-bound syntax)
 */
interface ReadonlyArray<T> {
  includes(searchElement: unknown, fromIndex?: number): searchElement is T;
}

/**
 * @see https://stackoverflow.com/questions/52856496/typescript-object-keys-return-string \
 * and countless github issues
 */
interface ObjectConstructor {
  keys<T>(obj: T): (keyof T)[];
}
