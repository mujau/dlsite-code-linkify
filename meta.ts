import {
  AUTHOR,
  SCRIPT_NAME,
  SCRIPT_NAME_HUMAN,
  VERSION,
} from "./constants.ts";

const home = `https://gitlab.com/${AUTHOR}/${SCRIPT_NAME}`;
const support = `${home}/-/issues`;
const download = `${home}/-/raw/master/dist/${SCRIPT_NAME}.user.js`;
const icon = `${home}/-/raw/1.2.0/img/fumicon.png`;
const description = "Linkifies DLsite work and creator codes.";

export const getMeta = (isDev: boolean): string =>
  `// ==UserScript==
// @name        ${SCRIPT_NAME_HUMAN}
// @namespace   green-is-my-paddy
// @version     ${VERSION}
// @description ${description}
// @author      ${AUTHOR}
// @homepageURL ${home}
// @supportURL  ${support}
// @downloadURL ${download}
// @icon        ${icon}
// @grant       GM.xmlHttpRequest
// @grant       GM.registerMenuCommand
// @grant       GM.addStyle
// @run-at      document-end${
    isDev
      ? `
// @match       file://*/*`
      : ""
  }
// @match       *://boards.4chan.org/*
// @match       *://4chan.gay/*
// @match       *://arch.b4k.co/*
// @match       *://arch.b4k.dev/*
// @match       *://archived.moe/*
// @match       *://gazellegames.net/*
// @match       *://e-hentai.org/g/*
// @match       *://exhentai.org/g/*
// ==/UserScript==\n`;
