import { denoPlugins } from "@luca/esbuild-deno-loader";
import { emptyDir } from "@std/fs";
import esbuild from "esbuild";
import { SCRIPT_NAME } from "./constants.ts";
import { C } from "./macros/classNames.ts";
import style from "./macros/style.css.ts";
import { getMeta } from "./meta.ts";

const isDev = Deno.args.includes("--dev");
/** Latest tor browser. */
const target = ["firefox128"];
const charset = "utf8";

await emptyDir("./dist");

const css = (await esbuild.transform(style, {
  loader: "css",
  charset,
  target,
  minify: true,
})).code.trim();

await esbuild.build({
  entryPoints: ["./src/main.ts"],
  outfile: `./dist/${SCRIPT_NAME}.user.js`,
  platform: "neutral",
  charset,
  target,
  bundle: true,
  banner: {
    js: `${getMeta(isDev)}\n"use strict";`,
  },
  define: {
    __DEV: `${isDev}`,
    __CSS_SLOT: `"${css}"`,
    __CLASS_NAMES: `'${JSON.stringify(C)}'`,
  },
  plugins: [...denoPlugins()],
});

esbuild.stop();
